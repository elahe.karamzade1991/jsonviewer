// https://docs.cypress.io/api/introduction/api.html

describe("visit", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("label", "chose a json file");
  });
});
describe('upload file', () => {
  it('Testing json uploading', () => {
    cy.fixture('./test.json').then(fileContent => {
      cy.get('input[type="file"]').attachFile({
        fileContent: fileContent,
        fileName: 'test.json',
        mimeType: 'application/json'
      });
    });
  });
})