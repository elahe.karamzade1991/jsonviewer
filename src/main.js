import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import './index.css'
import JsonViewer from "vue3-json-viewer"
const app = createApp(App)

app.use(JsonViewer)

app.mount("#app");
